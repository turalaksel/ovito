from ovito.modifiers import CalculateDisplacementsModifier

mod = CalculateDisplacementsModifier()
mod.vis.enabled = True
mod.vis.color = (0,0,0)